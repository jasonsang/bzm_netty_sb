package com.fjb.pojo.im;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * 好友请求记录
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public class ImChatFriendRequestLogs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer mainUserId;

    private Integer userId;

    private Integer requestUserId;

    /**
     * 请求状态     1、通过     2、已拒绝   3、申请中
     */
    private Integer requestStatus;

    private LocalDateTime requestUpdateTime;

    /**
     * 查看状态      1、已查看   2、未查看	
     */
    private Integer lookStatus;

    /**
     * 查看时间
     */
    private LocalDateTime lookUpdateTime;

    private String requestRemark;

    private LocalDateTime createTime;

    private Integer createUserId;

    private LocalDateTime updateTime;

    private Integer updateUserId;

    /**
     * 数据状态   1、正常     2、删除
     */
    private Integer dataStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(Integer mainUserId) {
        this.mainUserId = mainUserId;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getRequestUserId() {
        return requestUserId;
    }

    public void setRequestUserId(Integer requestUserId) {
        this.requestUserId = requestUserId;
    }
    public Integer getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Integer requestStatus) {
        this.requestStatus = requestStatus;
    }
    public LocalDateTime getRequestUpdateTime() {
        return requestUpdateTime;
    }

    public void setRequestUpdateTime(LocalDateTime requestUpdateTime) {
        this.requestUpdateTime = requestUpdateTime;
    }
    public Integer getLookStatus() {
        return lookStatus;
    }

    public void setLookStatus(Integer lookStatus) {
        this.lookStatus = lookStatus;
    }
    public LocalDateTime getLookUpdateTime() {
        return lookUpdateTime;
    }

    public void setLookUpdateTime(LocalDateTime lookUpdateTime) {
        this.lookUpdateTime = lookUpdateTime;
    }
    public String getRequestRemark() {
        return requestRemark;
    }

    public void setRequestRemark(String requestRemark) {
        this.requestRemark = requestRemark;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public Integer getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Integer createUserId) {
        this.createUserId = createUserId;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public Integer getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }
    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "ImChatFriendRequestLogs{" +
        "id=" + id +
        ", mainUserId=" + mainUserId +
        ", userId=" + userId +
        ", requestUserId=" + requestUserId +
        ", requestStatus=" + requestStatus +
        ", requestUpdateTime=" + requestUpdateTime +
        ", lookStatus=" + lookStatus +
        ", lookUpdateTime=" + lookUpdateTime +
        ", requestRemark=" + requestRemark +
        ", createTime=" + createTime +
        ", createUserId=" + createUserId +
        ", updateTime=" + updateTime +
        ", updateUserId=" + updateUserId +
        ", dataStatus=" + dataStatus +
        "}";
    }
}
