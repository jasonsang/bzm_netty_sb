package com.fjb.pojo.im;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * <p>
 * im_chat聊天组信息
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public class ImChatGroupInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer mainUserId;

    private Integer userId;

    private Integer friendInfoId;

    /**
     * 组编号
     */
    private String groupNumber;

    /**
     * 组名称
     */
    private String groupName;

    /**
     * 组标签
     */
    private String groupTag;

    private String createUserId;

    private LocalDateTime createTime;

    private String updateUserId;

    private LocalDateTime updateTime;

    /**
     * 组备注
     */
    private String groupRemark;

    /**
     * 数据状态   1、正常     2、删除
     */
    private Integer dataStatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(Integer mainUserId) {
        this.mainUserId = mainUserId;
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Integer getFriendInfoId() {
        return friendInfoId;
    }

    public void setFriendInfoId(Integer friendInfoId) {
        this.friendInfoId = friendInfoId;
    }
    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String getGroupTag() {
        return groupTag;
    }

    public void setGroupTag(String groupTag) {
        this.groupTag = groupTag;
    }
    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
    public String getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(String updateUserId) {
        this.updateUserId = updateUserId;
    }
    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
    public String getGroupRemark() {
        return groupRemark;
    }

    public void setGroupRemark(String groupRemark) {
        this.groupRemark = groupRemark;
    }
    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Override
    public String toString() {
        return "ImChatGroupInfo{" +
        "id=" + id +
        ", mainUserId=" + mainUserId +
        ", userId=" + userId +
        ", friendInfoId=" + friendInfoId +
        ", groupNumber=" + groupNumber +
        ", groupName=" + groupName +
        ", groupTag=" + groupTag +
        ", createUserId=" + createUserId +
        ", createTime=" + createTime +
        ", updateUserId=" + updateUserId +
        ", updateTime=" + updateTime +
        ", groupRemark=" + groupRemark +
        ", dataStatus=" + dataStatus +
        "}";
    }
}
