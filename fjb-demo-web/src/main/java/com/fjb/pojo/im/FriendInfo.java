package com.fjb.pojo.im;

/**
 * @Description:好友信息对象
 * @author hemiao
 * @time:2020年5月27日 下午10:24:14
 */
public class FriendInfo {

	/**
	 * 好友id
	 */
	private String friendId;
	
	/**
	 * 好友名称
	 */
	private String nickname;
	
	/**
	 * 好友备注
	 */
	private String friendRemark;

	public String getFriendId() {
		return friendId;
	}

	public void setFriendId(String friendId) {
		this.friendId = friendId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getFriendRemark() {
		return friendRemark;
	}

	public void setFriendRemark(String friendRemark) {
		this.friendRemark = friendRemark;
	}
}
